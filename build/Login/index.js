(function(){
      
  var createPageHandler = function() {
    return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	var $app_template$ = __webpack_require__(12)
	var $app_style$ = __webpack_require__(13)
	var $app_script$ = __webpack_require__(14)
	
	$app_define$('@app-component/index', [], function($app_require$, $app_exports$, $app_module$){
	     $app_script$($app_module$, $app_exports$, $app_require$)
	     if ($app_exports$.__esModule && $app_exports$.default) {
	            $app_module$.exports = $app_exports$.default
	        }
	     $app_module$.exports.template = $app_template$
	     $app_module$.exports.style = $app_style$
	})
	
	$app_bootstrap$('@app-component/index',{ packagerVersion: '0.0.5'})


/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var fetch = $app_require$('@app-module/system.fetch');
	var prompt = $app_require$('@app-module/system.prompt');
	
	var http = exports.http = {
	    GET: function GET(url, param, callback) {
	        fetch.fetch({
	            url: url,
	            method: 'GET',
	            data: param,
	            success: function success(res) {
	                console.log(res.data);
	                callback(res.data);
	            },
	            fail: function fail(res, code) {
	                console.log(url + ":\n" + res);
	                callback(res);
	            }
	        });
	    },
	
	    getMethod: function getMethod(url, param, callback) {
	        fetch.fetch({
	            url: url,
	            method: 'GET',
	            data: JSON.stringify(param),
	            contentType: 'application/json',
	            success: function success(res) {
	                console.log(JSON.stringify(res.data.sendData));
	                callback(res.data.sendData);
	            },
	            fail: function fail(res, code) {
	                console.log(url + ":\n" + res);
	                callback(res.data.sendDate);
	            }
	        });
	    },
	
	    postMethod: function postMethod(url, param, callback) {
	        console.log('[POST] ' + url);
	        console.log('[PARAM]  ' + JSON.stringify(param));
	        fetch.fetch({
	            url: url,
	            method: 'POST',
	            header: { 'Content-Type': 'application/json;charset=UTF-8' },
	            data: JSON.stringify(param),
	            success: function success(res) {
	                console.log('[CALLBACK] ' + res.data);
	                var obj = JSON.parse(res.data);
	                if (obj.e.code === 0) {
	                    callback(obj.data.sendData);
	                } else {
	                    prompt.showToast({
	                        message: obj.e.desc
	                    });
	                }
	            },
	            fail: function fail(res, code) {
	                console.log(res + " | " + "code= " + code);
	                callback(res);
	            }
	        });
	    }
	};

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var IP = "http://oaapi.yingfeng365.com/jsyf-oa";
	var URL = exports.URL = {
	    //登录
	    loginIn: IP + "/user/login.json",
	
	    //签到
	    signIn: IP + "/signIn/save.json",
	
	    //签到撤销
	    revertIn: IP + "/signIn/revertIn.json",
	
	    //签退撤销
	    revertOut: IP + "/signIn/revert.json",
	
	    //百度逆地址解析
	    getAddress: "http://api.map.baidu.com/geocoder/v2/?callback=&output=json&pois=0&ak=18eZsB5LvSoIf1HZOCUBDXKWHMbvhfpa&location=",
	
	    //查询签到
	    getSign: IP + "/signIn/getSignInByDay.json",
	
	    //上传文件
	    uploadFile: IP + "/attach/uploadFile.json",
	
	    //获取推荐
	    getRecommend: "https://www.apiopen.top/satinApi",
	
	    //获取美图
	    getPic: "https://www.apiopen.top/meituApi"
	
	};

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports) {

	module.exports = {
	  "type": "div",
	  "attr": {},
	  "classList": [
	    "center"
	  ],
	  "children": [
	    {
	      "type": "image",
	      "attr": {
	        "src": "../Common/ic_launcher.png"
	      },
	      "style": {
	        "width": "160px",
	        "height": "160px",
	        "marginTop": "60px"
	      }
	    },
	    {
	      "type": "text",
	      "attr": {
	        "value": "OA移动办公系统"
	      },
	      "classList": [
	        "title"
	      ]
	    },
	    {
	      "type": "input",
	      "attr": {
	        "type": "number",
	        "value": function () {return this.tel},
	        "placeholder": "请输入手机号"
	      },
	      "style": {
	        "marginTop": "50px"
	      },
	      "events": {
	        "change": function (evt) {this.onTelChange(evt)}
	      }
	    },
	    {
	      "type": "text",
	      "attr": {},
	      "classList": [
	        "line"
	      ]
	    },
	    {
	      "type": "input",
	      "attr": {
	        "type": "password",
	        "placeholder": "请输入密码",
	        "value": function () {return this.pwd}
	      },
	      "events": {
	        "change": function (evt) {this.onPwdChange(evt)}
	      }
	    },
	    {
	      "type": "text",
	      "attr": {},
	      "classList": [
	        "line"
	      ]
	    },
	    {
	      "type": "input",
	      "attr": {
	        "type": "button",
	        "value": "登录"
	      },
	      "classList": [
	        "btn"
	      ],
	      "events": {
	        "click": function (evt) {this.goLogin(evt)}
	      }
	    }
	  ]
	}

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = {
	  ".doc-page": {
	    "flex": 1,
	    "flexDirection": "column"
	  },
	  ".page-title-wrap": {
	    "paddingTop": "50px",
	    "paddingBottom": "80px",
	    "justifyContent": "center"
	  },
	  ".page-title": {
	    "paddingTop": "30px",
	    "paddingBottom": "30px",
	    "paddingLeft": "40px",
	    "paddingRight": "40px",
	    "borderTopColor": "#bbbbbb",
	    "borderRightColor": "#bbbbbb",
	    "borderBottomColor": "#bbbbbb",
	    "borderLeftColor": "#bbbbbb",
	    "color": "#bbbbbb",
	    "borderBottomWidth": "2px"
	  },
	  ".btn": {
	    "height": "100px",
	    "textAlign": "center",
	    "borderRadius": "5px",
	    "marginRight": "60px",
	    "marginLeft": "60px",
	    "marginBottom": "50px",
	    "color": "#ffffff",
	    "fontSize": "30px",
	    "backgroundColor": "#2c8cec",
	    "marginTop": "150px",
	    "fontWeight": "bold"
	  },
	  ".row": {
	    "display": "flex",
	    "flexDirection": "row"
	  },
	  ".col": {
	    "display": "flex",
	    "flexDirection": "column"
	  },
	  ".progressDiv": {
	    "position": "fixed",
	    "top": "350px",
	    "left": "280px",
	    "flexDirection": "column",
	    "justifyContent": "center",
	    "alignItems": "center",
	    "textAlign": "center",
	    "backgroundColor": "#6d6e6a",
	    "borderRadius": "20px",
	    "width": "200px",
	    "height": "200px",
	    "zIndex": 9999
	  },
	  ".center": {
	    "flexDirection": "column",
	    "alignItems": "center"
	  },
	  ".line": {
	    "backgroundColor": "#333333",
	    "height": "1px",
	    "width": "85%"
	  },
	  ".title": {
	    "fontSize": "55px",
	    "textAlign": "center",
	    "fontWeight": "bold",
	    "marginTop": "20px",
	    "color": "#333333"
	  },
	  "input": {
	    "width": "85%",
	    "paddingTop": "25px",
	    "paddingRight": "25px",
	    "paddingBottom": "25px",
	    "paddingLeft": "25px",
	    "textAlign": "left"
	  },
	  ".btnClear": {
	    "marginTop": "150px",
	    "marginBottom": "30px",
	    "fontSize": "24px",
	    "color": "#999999",
	    "textDecoration": "underline"
	  }
	}

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = function(module, exports, $app_require$){'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _system = $app_require$('@app-module/system.network');
	
	var _system2 = _interopRequireDefault(_system);
	
	var _system3 = $app_require$('@app-module/system.prompt');
	
	var _system4 = _interopRequireDefault(_system3);
	
	var _system5 = $app_require$('@app-module/system.storage');
	
	var _system6 = _interopRequireDefault(_system5);
	
	var _system7 = $app_require$('@app-module/system.router');
	
	var _system8 = _interopRequireDefault(_system7);
	
	var _common = __webpack_require__(7);
	
	var _UrlConfig = __webpack_require__(8);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	    private: {
	        tel: '',
	        pwd: ''
	    },
	    onInit: function onInit() {
	        this.$page.setTitleBar({ text: '登录' });
	    },
	    onReady: function onReady() {
	        var that = this;
	        _system2.default.getType({
	            success: function success(data) {
	                if (data.type == 'none') {
	                    _system4.default.showToast({
	                        message: '\u5F53\u524D\u65E0\u7F51\u7EDC,\u8BF7\u68C0\u67E5\u8BBE\u7F6E'
	                    });
	                } else {
	                    that.autoLogin();
	                }
	            }
	        });
	    },
	    onTelChange: function onTelChange(e) {
	        this.tel = e.value === undefined ? '' : e.value;
	    },
	    onPwdChange: function onPwdChange(e) {
	        this.pwd = e.value === undefined ? '' : e.value;
	    },
	    autoLogin: function autoLogin() {
	        var that = this;
	        _system6.default.get({
	            key: 'tel',
	            success: function success(tel) {
	                if (tel) {
	                    that.tel = tel;
	                    _system6.default.get({
	                        key: 'pwd',
	                        success: function success(pwd) {
	                            if (pwd) {
	                                that.pwd = pwd;
	                                that.goLogin();
	                            }
	                        }
	                    });
	                }
	            }
	        });
	    },
	    clearLogin: function clearLogin() {
	        _system6.default.clear({
	            success: function success(data) {
	                _system4.default.showToast({
	                    message: '\u6E05\u9664\u6210\u529F'
	                });
	            },
	            fail: function fail(data, code) {
	                _system4.default.showToast({
	                    message: '\u6E05\u9664\u5931\u8D25'
	                });
	            }
	        });
	    },
	    goLogin: function goLogin() {
	        var that = this;
	        _system2.default.getType({
	            success: function success(data) {
	                if (data.type == 'none') {
	                    _system4.default.showToast({
	                        message: '\u5F53\u524D\u65E0\u7F51\u7EDC,\u8BF7\u68C0\u67E5\u8BBE\u7F6E'
	                    });
	                } else {
	                    that.doGoLogin();
	                }
	            }
	        });
	    },
	    doGoLogin: function doGoLogin() {
	        var that = this;
	        var param = {
	            tel: that.tel,
	            pwd: that.pwd
	        };
	        _common.http.postMethod(_UrlConfig.URL.loginIn, param, function (res) {
	            that.$app.userInfo = res;
	
	            _system6.default.set({
	                key: 'tel',
	                value: that.tel
	            });
	            _system6.default.set({
	                key: 'pwd',
	                value: that.pwd
	            });
	            _system6.default.set({
	                key: 'userInfo',
	                value: res[0],
	                success: function success(data) {
	                    _system8.default.replace({
	                        uri: 'Demo'
	                    });
	                },
	                fail: function fail(data, code) {
	                    console.log("storage fail, code=" + code);
	                }
	            });
	        });
	    }
	};
	
	
	var moduleOwn = exports.default || module.exports;
	var accessors = ['public', 'protected', 'private'];
	
	if (moduleOwn.data && accessors.some(function (acc) {
	    return moduleOwn[acc];
	})) {
	    throw new Error('页面VM对象中的属性data不可与"' + accessors.join(',') + '"同时存在，请使用private替换data名称');
	} else if (!moduleOwn.data) {
	    moduleOwn.data = {};
	    moduleOwn._descriptor = {};
	    accessors.forEach(function (acc) {
	        var accType = _typeof(moduleOwn[acc]);
	        if (accType === 'object') {
	            moduleOwn.data = Object.assign(moduleOwn.data, moduleOwn[acc]);
	            for (var name in moduleOwn[acc]) {
	                moduleOwn._descriptor[name] = { access: acc };
	            }
	        } else if (accType === 'function') {
	            console.warn('页面VM对象中的属性' + acc + '的值不能是函数，请使用对象');
	        }
	    });
	}}

/***/ })
/******/ ]);
  };
  if (typeof window === "undefined") {
    return createPageHandler();
  }
  else {
    window.createPageHandler = createPageHandler
  }
})();
//# sourceMappingURL=index.js.map